package main
/* You need to compile this to make it work */
/* render <yaml> <template> */

import (
  "fmt"
  "gopkg.in/yaml.v2"
  "io/ioutil"
  "text/template"
  "bytes"
  "os"
  "regexp"
)

type Subsection struct {
  Title string
  Body string
}

type Section struct {
  Title string
  Body string
  Subsections []Subsection
}

type Chapter struct {
  Title string
  Body string
  Sections []Section
}

type Book struct {
  Include_folder string
  Page_title string
  Page_color string
  Book_name string
  Last_updated string
  Introduction string
  Chapters []Chapter
}

func check(e error) {
  if e != nil {
      panic(e)
  }
}

var re_html = regexp.MustCompile(`\.html$`)

func main() {
  var buf bytes.Buffer

  if len(os.Args) != 3 {
    panic(fmt.Errorf("Wrong amount of parameters. render <Book> <Template>\n"))
  }

  /* read in book in yaml format */
  d, err := ioutil.ReadFile(os.Args[1]); check(err)

  /* parse yaml  */
  mybook := Book{}
  err = yaml.Unmarshal(d, &mybook); check(err)
  /* We need to preprocess d before Unmarshalling */
  if mybook.Include_folder != "" {
    includes := make(map[string]string)
    var prebuf bytes.Buffer
    dir, err := ioutil.ReadDir(mybook.Include_folder); check(err)
    for _, dirEntry := range dir {
      isHtml, err := regexp.MatchString(`\.html$`,dirEntry.Name()); check(err)
      if (dirEntry.IsDir()==false&&isHtml) {
        include_key := re_html.ReplaceAllString(dirEntry.Name(),"")
        data, err := ioutil.ReadFile(mybook.Include_folder+"/"+dirEntry.Name()); check(err)
        includes[include_key] = string(data)
      }
    }
    pre, err := template.ParseFiles(os.Args[1]); check(err)
    err = pre.Execute(&prebuf, includes); check(err)
    err = yaml.Unmarshal(prebuf.Bytes(), &mybook); check(err)
  }

  /* read in template in html format */
  tmpl, err := template.ParseFiles(os.Args[2]); check(err)

  /* pair the two together */
  err = tmpl.Execute(&buf, mybook); check(err)

  /* output to stdout */
  fmt.Println(buf.String())
}
